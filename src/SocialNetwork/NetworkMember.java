package SocialNetwork;

import java.util.ArrayList;
import java.util.Observable;
import java.util.AbstractMap.SimpleEntry;

import jade.util.leap.HashMap;

import java.util.Iterator;
import java.util.List;

public class NetworkMember extends Observable {
  int nodeId;
  SimpleEntry<Integer, String> pair;
  List<NetworkMember> neighbors = new ArrayList<NetworkMember>();

  public NetworkMember (int NodeId) {
    this.nodeId = NodeId;
  }

  //set methods
  public void setNodeId (int NodeId) {
    this.nodeId = NodeId;;
    this.setChanged();
  }

  public void addChange (Integer n, String change) {
    System.out.println("The Network member has changed");
    pair = new SimpleEntry<Integer, String>(n, change);
    setChanged();
    notifyObservers(pair);
  }
}
