package SocialNetwork;

import java.util.AbstractMap.SimpleEntry;

public class TwitterMember extends NetworkMember {
  int tweetcount;
  int followers;
  int followees;
  SimpleEntry<Integer, String> pair;

  public TwitterMember (int NodeId) {
    super(NodeId);
    this.tweetcount = 0;
    this.followers = 0;
    this.followees = 0;
    // TODO Auto-generated constructor stub
  }

  public void addReply (TwitterMember n) {
    // add a reply for n
  }

  public void addTweet () {
    this.tweetcount++;
  }

  public void addFollowees () {
    this.followees++;
  }

  public void addFollower () {
    this.followers++;
  }

  public void setFollowers (int followers) {
    this.followers = followers;
  }

  public void setFollowees (int followees) {
    this.followees = followees;
  }

  //get methods
  public int getNodeId () {
    return nodeId;
  }

  public int getTweetcount () {
    return tweetcount;
  }

  public int getFollowers () {
    return followers;
  }

  public int getFollowees () {
    return followees;
  }

  public void addChange (Integer n, String change) {
    System.out.println("The Network member has changed");
    pair = new SimpleEntry<Integer, String>(n, change);
    setChanged();
    notifyObservers(pair);
  }
}
