package SocialNetwork;

import java.util.HashMap;
import java.util.Set;

public class NetworkData {
  protected HashMap<String, Integer> data;

  public NetworkData () {
    data = new HashMap<String, Integer>();
  }

  public Integer getValue (String key) {
    return data.get(key);
  }

  public void put (String key, Integer i) {
    data.put(key, i);
  }

  public void remove (String key) {
    data.remove(key);
  }

  public void resetValue (String key) {
    this.remove(key);;
    this.put(key, 0);
  }

  public void increment (String key) {
    Integer j = this.getValue(key);
    this.remove(key);;
    this.put(key, j + 1);
  }

  public void reset () {
    Set<String> keys = this.data.keySet();
    data = new HashMap<String, Integer>();
    for (String key : keys) {
      System.out.println("Key " + key + " est remise à zero");
      data.put(key, 0);
    } ;
  }

  public Double agregate () {
    double v = 0;
    for (String key : data.keySet()) {
      System.out.println("Key " + key + " est remise à zero");
      v = +this.getValue(key);
    } ;
    return v;
  }
}
