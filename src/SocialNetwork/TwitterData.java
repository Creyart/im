package SocialNetwork;

public class TwitterData extends NetworkData {
  int replycount;
  int retweetcount;
  int mentioncount;

  public TwitterData () {
    super();
    super.put("replycount", 0);
    super.put("retweetcount", 0);
    super.put("mentioncount", 0);
  }
}
