package Main;

import java.util.ArrayList;

import Agents.*;
import Graph.Node;
import SocialNetwork.NetworkMember;
import SocialNetwork.TwitterMember;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;

public class Main {
  public static void main (String[] args) {
    Runtime runtime = Runtime.instance();
    Profile p = new ProfileImpl();
    p.setParameter(Profile.MAIN_HOST, "localhost");
    p.setParameter(Profile.MAIN_PORT, "1099");
    p.setParameter(Profile.GUI, "true");
    int numberOfNodes = 4;
    Node n;
    TwitterMember nm;
    GraphUpdate gu;
    DataAnalysis da;
    Agent1 ag;
    ArrayList<TwitterMember> socialNetwork = new ArrayList<TwitterMember>();
    ContainerController containerController = runtime.createMainContainer(p);
    AgentController policeAgentController;
    for (int i = 1; i <= numberOfNodes; i++) {
      nm = new TwitterMember(i);
      n = new Node(i);
      da = new DataAnalysis(nm);
      gu = new GraphUpdate(n, nm, da);
      ag = new Agent1(nm, n, gu);
      socialNetwork.add(nm);
      /*   policeAgentController = containerController.createNewAgent("AgentNM"+i, "Agents.Agent1", null); */
      try {
        policeAgentController = containerController.acceptNewAgent("AgentNM" + i, ag);
        policeAgentController.start();
      }
      catch (StaleProxyException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    NetworkSimulator sim = new NetworkSimulator(socialNetwork);
    try {
      policeAgentController = containerController.acceptNewAgent("NetworkSimulator", sim);
      policeAgentController.start();
    }
    catch (StaleProxyException e) {
      e.printStackTrace();
    }
    System.out.println("*******SocialNetwork and Agents Created");
    System.out.println("***********END**********");
  }
}
