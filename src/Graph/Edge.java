package Graph;

import java.util.HashMap;

import SocialNetwork.NetworkData;

public class Edge {
  int i, j;
  NetworkData netdata;

  public Edge (int i, int j) {
    this.i = i;
    this.j = j;
  }

  public Edge (int i, int j, NetworkData netdata) {
    this.i = i;
    this.j = j;
    this.netdata = netdata;
  }

  public Integer getValue (String key) {
    return netdata.getValue(key);
  }

  public void put (String key, Integer i) {
    netdata.put(key, i);
  }

  public void remove (String key) {
    netdata.remove(key);
  }

  public void increment (String key) {
    Integer j = this.getValue(key);
    this.remove(key);;
    this.put(key, j + 1);
  }
}
