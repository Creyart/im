package Graph;

import java.util.HashMap;
import java.util.Hashtable;
import jade.util.leap.Map;

public class Node {
  public Integer nodeId;
  HashMap<Integer, Double> weights;

  public Node (Integer NodeId) {
    this.nodeId = NodeId;
    weights = new HashMap<Integer, Double>();
  }

  public void setNodeId (Integer NodeId) {
    this.nodeId = NodeId;
  }

  public Integer getNodeId () {
    return nodeId;
  }

  public void addNeighbor (Node n) {
    weights.put(n.getNodeId(), 0.0);
  }

  public void addNeighbor (Node n, Double wij) {
    weights.put(n.getNodeId(), wij);
  }

  public void updateNeighbor (Node n, Double wij) {
    weights.remove(n.getNodeId());
    weights.put(n.getNodeId(), wij);
  }

  public void removeNeighbor (Node n) {
    weights.remove(n.getNodeId());
  }

  public Double getEdgeWeight (Node n) {
    return (Double) weights.get(n.getNodeId());
  }
}
