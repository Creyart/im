package Agents;

import java.util.HashMap;

import Graph.Node;
import SocialNetwork.*;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;

public class GraphUpdate extends CyclicBehaviour {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  Node n;
  NetworkMember nm;
  NetworkMember oldnm;
  DataAnalysis da;

  public GraphUpdate (Agent a, DataAnalysis da) {
    super(a);
    this.da = da;
    // TODO Auto-generated constructor stub
  }

  public GraphUpdate (Agent a, Node n, NetworkMember nm, DataAnalysis da) {
    super(a);
    this.n = n;
    this.nm = nm;
    this.da = da;
  }

  public GraphUpdate (Node n, TwitterMember nm, DataAnalysis da) {
    super();
    this.n = n;
    this.nm = nm;
    this.da = da;
  }

  public void updateNetworkMember (TwitterMember nm) {
    this.oldnm = this.nm;
    this.nm = nm;
    System.out.println(n.nodeId + "will be upadted by GraphUpdater");
    //TODO : put here the needed data 	and update the node n according to the new nm and old nm	  
  }

  public void updateWeights () {
    //TODO : use the agregation for all the neighbors
    HashMap<Integer, TwitterData> observedData = da.getObservedData();
    NetworkData data;
    for (Integer key : observedData.keySet()) {
      data = observedData.get(key);
      //update the eidge this - key with da
      System.out.println("Update of the edge " + key + "  Value: " + data.agregate());
    }
  }

  public Double agregateDated (Node n) {
    //agregarate date of n
    return 0.0;
  }

  @Override
  public void action () {
    System.out.println("Graphe Update");
    this.updateWeights();
    da.reset();
    try {
      Thread.sleep(10);
    }
    catch (InterruptedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public DataAnalysis getData () {
    return da;
  }
  //@Override
  //public boolean done() {
  // TODO Auto-generated method stub
  //	return false;
  //}
}