package Agents;

import java.util.ArrayList;
import java.util.HashMap;

import Graph.Node;
import SocialNetwork.*;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;

public class NetworkSimulator extends Agent {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  ArrayList<TwitterMember> socialNetwork;
  int numberOfNodes;

  public NetworkSimulator (ArrayList<TwitterMember> socialNetwork) {
    this.socialNetwork = socialNetwork;
    numberOfNodes = socialNetwork.size();
  }

  @Override
  protected void setup () {
    this.addBehaviour(new SimBehaviour());
    //this.addBehaviour(new DataAnalysis(this));
    //this.addBehaviour(new UpdateGraph(this));
  }

  public class SimBehaviour extends CyclicBehaviour {
    NetworkMember nm;

    @Override
    public void action () {
      Integer random1 = (int) (Math.random() * (numberOfNodes - 1));
      Integer random2 = (int) (Math.random() * (numberOfNodes - 1));
      System.out.println("NNNNNNNNetwork member   " + random1 + "  a un événement");
      nm = (TwitterMember) socialNetwork.get(random1);
      nm.addChange(random2, "replycount");
      // nm.addChange(random2, "mentioncount");
      nm.addChange(random2, "retweetcount");
    }
  }
}
