package Agents;

import java.util.*;

import Graph.Node;
import SocialNetwork.NetworkData;
import SocialNetwork.NetworkMember;
import SocialNetwork.TwitterMember;
import jade.core.Agent;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;

public class Agent1 extends Agent {
  /**
  * 
  */
  private static final long serialVersionUID = 1L;
  boolean perception = false;
  //add social network member
  //pick this info from the network member
  NetworkMember nm; //=new NetworkMember(0,0,0,0,0,0,0);
  Node n;
  HashMap<Integer, NetworkData> observedData;
  DataAnalysis da;
  GraphUpdate g;

  public Agent1 (NetworkMember nm, Node n, DataAnalysis da, GraphUpdate g) {
    super();
    this.n = n;
    this.nm = nm;
    this.da = da;
    this.g = g;
    this.addBehaviour(g);
  }

  public Agent1 (TwitterMember nm, Node n, GraphUpdate g) {
    super();
    this.n = n;
    this.nm = nm;
    this.da = g.getData();
    this.g = g;
    this.addBehaviour(g);
  }

  @Override
  protected void setup () {
    //	this.addBehaviour(g);	
  }
}
