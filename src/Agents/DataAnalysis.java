package Agents;

import java.util.HashMap;
import SocialNetwork.*;
import java.util.Observable;
import java.util.Observer;
import java.util.AbstractMap.SimpleEntry;

import SocialNetwork.NetworkData;
import SocialNetwork.NetworkMember;

public class DataAnalysis implements Observer {
  //define a TwitterData for each neighbor
  HashMap<Integer, TwitterData> observedData;
  NetworkMember oldnm;
  NetworkMember nm; //=new NetworkMember(0,0,0,0,0,0,0);

  //NetworkMember a;
  public DataAnalysis (NetworkMember nm) //include observable arg in the constructor
  {
    this.nm = nm;
    observedData = new HashMap<Integer, TwitterData>();
    nm.addObserver(this);
  }

  //	public DataAnalysis() {	}
  public void setNetworkMember (NetworkMember nm) {
    this.oldnm = nm;
    this.nm = nm;
  }

  @Override
  public void update (Observable o, Object arg) {
    SimpleEntry<Integer, String> pair = (SimpleEntry<Integer, String>) arg;
    this.update(pair);
    System.out.println("Update neighbor :" + pair.getKey() + "and Date " + pair.getValue());
  }

  public void update (SimpleEntry<Integer, String> pair) {
    System.out.println("The observer is informed");
    Integer key = pair.getKey();
    String attribute = pair.getValue();
    if (observedData.containsKey(key)) {
      observedData.get(key).increment(attribute);
    }
    else {
      observedData.put(key, new TwitterData());
    }
  }

  public void reset () {
    for (Integer key : observedData.keySet()) {
      observedData.get(key).reset();
    }
  }

  public HashMap<Integer, TwitterData> getObservedData () {
    return observedData;
  }
}